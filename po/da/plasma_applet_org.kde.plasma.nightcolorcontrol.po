# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Martin Schlander <mschlander@opensuse.org>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-30 08:46+0000\n"
"PO-Revision-Date: 2020-01-30 20:30+0100\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <kde-i18n-doc@kde.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.3\n"

#: package/contents/ui/main.qml:32
#, kde-format
msgid "Night Color Control"
msgstr "Kontrol af natfarve"

#: package/contents/ui/main.qml:35
#, kde-format
msgid "Night Color is inhibited"
msgstr "Natfarver undertrykkes"

#: package/contents/ui/main.qml:38
#, kde-format
msgid "Night Color is unavailable"
msgstr "Natfarver er ikke tilgængelige"

#: package/contents/ui/main.qml:41
#, fuzzy, kde-format
#| msgid "Night Color is disabled"
msgid "Night Color is disabled. Click to configure"
msgstr "Natfarver er deaktiveret"

#: package/contents/ui/main.qml:44
#, kde-format
msgid "Night Color is not running"
msgstr "Natfarver kører ikke"

#: package/contents/ui/main.qml:46
#, kde-format
msgid "Night Color is active (%1K)"
msgstr "Natfarver er aktive (%1K)"

#: package/contents/ui/main.qml:97
#, fuzzy, kde-format
#| msgid "Configure Night Color..."
msgid "&Configure Night Color…"
msgstr "Indstil natfarver..."
