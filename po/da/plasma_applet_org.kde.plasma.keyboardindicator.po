# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Schlander <mschlander@opensuse.org>, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-19 00:47+0000\n"
"PO-Revision-Date: 2019-05-07 13:41+0100\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <kde-i18n-doc@kde.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Keys"
msgstr "Taster"

#: contents/ui/configAppearance.qml:34
#, kde-format
msgctxt ""
"@label show keyboard indicator when Caps Lock or Num Lock is activated"
msgid "Show when activated:"
msgstr ""

#: contents/ui/configAppearance.qml:37
#, kde-format
msgctxt "@option:check"
msgid "Caps Lock"
msgstr "Lås skift"

#: contents/ui/configAppearance.qml:44
#, kde-format
msgctxt "@option:check"
msgid "Num Lock"
msgstr "Num Lock"

#: contents/ui/main.qml:30
#, fuzzy, kde-format
#| msgid "Caps Lock"
msgid "Caps Lock activated\n"
msgstr "Caps Lock"

#: contents/ui/main.qml:31
#, kde-format
msgid "Num Lock activated\n"
msgstr ""

#: contents/ui/main.qml:111
#, kde-format
msgid "No lock keys activated"
msgstr ""

#~ msgid "Num Lock"
#~ msgstr "Num Lock"

#~ msgid "%1: Locked\n"
#~ msgstr "%1: Låst\n"

#~ msgid "Unlocked"
#~ msgstr "Ikke låst"
