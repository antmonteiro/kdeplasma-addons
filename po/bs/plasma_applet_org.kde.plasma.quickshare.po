# Bosnian translation for bosnianuniversetranslation
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the bosnianuniversetranslation package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: bosnianuniversetranslation\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-02-03 00:45+0000\n"
"PO-Revision-Date: 2015-02-05 01:22+0000\n"
"Last-Translator: Dino Babahmetovic <dbabahmeto1@etf.unsa.ba>\n"
"Language-Team: Bosnian <bs@li.org>\n"
"Language: bs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2015-02-05 07:28+0000\n"
"X-Generator: Launchpad (build 17331)\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#: contents/config/config.qml:13
#, fuzzy, kde-format
#| msgid "General"
msgctxt "@title"
msgid "General"
msgstr "Opšte"

#: contents/ui/main.qml:225
#, fuzzy, kde-format
#| msgid "Paste"
msgctxt "@action"
msgid "Paste"
msgstr "Umetni"

#: contents/ui/main.qml:253 contents/ui/main.qml:293
#, kde-format
msgid "Share"
msgstr "Podijeli"

#: contents/ui/main.qml:254 contents/ui/main.qml:294
#, kde-format
msgid "Drop text or an image onto me to upload it to an online service."
msgstr "Prebaci tekst ili sliku na mene da ga pošaljem na online uslugu."

#: contents/ui/main.qml:294
#, kde-format
msgid "Upload %1 to an online service"
msgstr ""

#: contents/ui/main.qml:307
#, fuzzy, kde-format
#| msgid "Sending..."
msgid "Sending…"
msgstr "Šaljem..."

#: contents/ui/main.qml:308
#, kde-format
msgid "Please wait"
msgstr "Molim sačekajte"

#: contents/ui/main.qml:315
#, kde-format
msgid "Successfully uploaded"
msgstr "Uspješno poslano"

#: contents/ui/main.qml:316
#, kde-format
msgid "<a href='%1'>%1</a>"
msgstr "<a href='%1'>%1</a>"

#: contents/ui/main.qml:323
#, fuzzy, kde-format
#| msgid "Error during upload. Try again."
msgid "Error during upload."
msgstr "Greška tokom slanja. Pokušajte ponovo."

#: contents/ui/main.qml:324
#, kde-format
msgid "Please, try again."
msgstr "Molim, pokušajte ponovo."

#: contents/ui/settingsGeneral.qml:21
#, fuzzy, kde-format
#| msgid "History Size:"
msgctxt "@label:spinbox"
msgid "History size:"
msgstr "Veličina historije:"

#: contents/ui/settingsGeneral.qml:31
#, kde-format
msgctxt "@option:check"
msgid "Copy automatically:"
msgstr ""

#: contents/ui/ShareDialog.qml:31
#, kde-format
msgid "Shares for '%1'"
msgstr ""

#: contents/ui/ShowUrlDialog.qml:42
#, kde-format
msgid "The URL was just shared"
msgstr ""

#: contents/ui/ShowUrlDialog.qml:48
#, kde-format
msgctxt "@option:check"
msgid "Don't show this dialog, copy automatically."
msgstr ""

#: contents/ui/ShowUrlDialog.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Close"
msgstr ""

#~ msgid "Text Service:"
#~ msgstr "Tekstualna usluga:"

#~ msgid "Image Service:"
#~ msgstr "Slikovna usluga:"
