# Translation of plasma_applet_timer into Japanese.
# This file is distributed under the same license as the kdeplasma-addons package.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2008, 2009.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_timer\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-01 00:48+0000\n"
"PO-Revision-Date: 2021-10-17 13:41-0700\n"
"Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Poedit 2.4.2\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "外観"

#: package/contents/config/config.qml:19
#, kde-format
msgctxt "@title"
msgid "Predefined Timers"
msgstr "プリセットタイマー"

#: package/contents/config/config.qml:24
#, kde-format
msgctxt "@title"
msgid "Advanced"
msgstr "高度"

#: package/contents/ui/CompactRepresentation.qml:134
#, fuzzy, kde-format
#| msgid "Timer"
msgctxt "@action:button"
msgid "Pause Timer"
msgstr "タイマー"

#: package/contents/ui/CompactRepresentation.qml:134
#, fuzzy, kde-format
#| msgid "Timer"
msgctxt "@action:button"
msgid "Start Timer"
msgstr "タイマー"

#: package/contents/ui/CompactRepresentation.qml:191
#: package/contents/ui/CompactRepresentation.qml:208
#, kde-format
msgctxt "remaining time"
msgid "%1s"
msgid_plural "%1s"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/configAdvanced.qml:22
#, kde-format
msgctxt "@title:label"
msgid "After timer completes:"
msgstr "タイマーが完了したとき:"

#: package/contents/ui/configAdvanced.qml:26
#, kde-format
msgctxt "@option:check"
msgid "Execute command:"
msgstr "コマンドを実行:"

#: package/contents/ui/configAppearance.qml:30
#, kde-format
msgctxt "@title:label"
msgid "Display:"
msgstr "表示:"

#: package/contents/ui/configAppearance.qml:36
#, kde-format
msgctxt "@option:check"
msgid "Show title:"
msgstr "タイトルを表示:"

#: package/contents/ui/configAppearance.qml:53
#, kde-format
msgctxt "@option:check"
msgid "Show remaining time"
msgstr ""

#: package/contents/ui/configAppearance.qml:59
#, kde-format
msgctxt "@option:check"
msgid "Show seconds"
msgstr "秒を表示"

#: package/contents/ui/configAppearance.qml:64
#, fuzzy, kde-format
#| msgctxt "@option:check"
#| msgid "Show title:"
msgctxt "@option:check"
msgid "Show timer toggle"
msgstr "タイトルを表示:"

#: package/contents/ui/configAppearance.qml:69
#, kde-format
msgctxt "@option:check"
msgid "Show progress bar"
msgstr ""

#: package/contents/ui/configAppearance.qml:81
#, kde-format
msgctxt "@title:label"
msgid "Notifications:"
msgstr "通知:"

#: package/contents/ui/configAppearance.qml:85
#, kde-format
msgctxt "@option:check"
msgid "Show notification text:"
msgstr "通知テキストを表示:"

#: package/contents/ui/configTimes.qml:76
#, kde-format
msgid ""
"If you add predefined timers here, they will appear in plasmoid context menu."
msgstr ""
"ここにプリセットタイマーを追加すると Plasmoid のコンテキストメニューに表示さ"
"れます。"

#: package/contents/ui/configTimes.qml:83
#, kde-format
msgid "Add"
msgstr "追加"

#: package/contents/ui/configTimes.qml:120
#, kde-format
msgid "Scroll over digits to change time"
msgstr "数字をスクロールして時間を変更"

#: package/contents/ui/configTimes.qml:126
#, kde-format
msgid "Apply"
msgstr "適用"

#: package/contents/ui/configTimes.qml:134
#, kde-format
msgid "Cancel"
msgstr "キャンセル"

#: package/contents/ui/configTimes.qml:143
#, kde-format
msgid "Edit"
msgstr "編集"

#: package/contents/ui/configTimes.qml:152
#, kde-format
msgid "Delete"
msgstr "削除"

#: package/contents/ui/main.qml:65
#, kde-format
msgid "%1 is running"
msgstr "%1 は動いています"

#: package/contents/ui/main.qml:67
#, kde-format
msgid "%1 not running"
msgstr "%1 は動いていません"

#: package/contents/ui/main.qml:71
#, kde-format
msgid "Remaining time left: %1 second"
msgid_plural "Remaining time left: %1 seconds"
msgstr[0] "残り時間: %1 秒"
msgstr[1] "残り時間: %1 秒"

#: package/contents/ui/main.qml:71
#, kde-format
msgid ""
"Use mouse wheel to change digits or choose from predefined timers in the "
"context menu"
msgstr ""
"マウスホイールを使用して数字を変更するか、コンテキストメニューでプリセットタ"
"イマーから選択"

#: package/contents/ui/main.qml:128
#, kde-format
msgid "Timer"
msgstr "タイマー"

#: package/contents/ui/main.qml:130
#, kde-format
msgid "Timer finished"
msgstr "タイマー終了"

#: package/contents/ui/main.qml:146
#, kde-format
msgctxt "@action"
msgid "&Start"
msgstr "開始(&S)"

#: package/contents/ui/main.qml:147
#, kde-format
msgctxt "@action"
msgid "S&top"
msgstr "終了(&T)"

#: package/contents/ui/main.qml:148
#, kde-format
msgctxt "@action"
msgid "&Reset"
msgstr "リセット(&R)"
