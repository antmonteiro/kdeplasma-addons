# translation of konsoleprofiles.po to Low Saxon
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Manfred Wiese <m.j.wiese@web.de>, 2012, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: konsoleprofiles\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-16 00:49+0000\n"
"PO-Revision-Date: 2014-03-19 10:03+0100\n"
"Last-Translator: Manfred Wiese <m.j.wiese@web.de>\n"
"Language-Team: Low Saxon <kde-i18n-nds@kde.org>\n"
"Language: nds\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/ui/konsoleprofiles.qml:61
#, fuzzy, kde-format
#| msgid "Konsole Profiles"
msgctxt "@title"
msgid "Konsole Profiles"
msgstr "Konsole-Profilen"

#: package/contents/ui/konsoleprofiles.qml:81
#, kde-format
msgid "Arbitrary String Which Says Something"
msgstr "Jichtenseen beschrieven Tekenkeed"
