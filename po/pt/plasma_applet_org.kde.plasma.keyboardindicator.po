msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-19 00:47+0000\n"
"PO-Revision-Date: 2022-05-16 13:59+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Lock Caps\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Keys"
msgstr "Teclas"

#: contents/ui/configAppearance.qml:34
#, kde-format
msgctxt ""
"@label show keyboard indicator when Caps Lock or Num Lock is activated"
msgid "Show when activated:"
msgstr "Mostrar quando activo:"

#: contents/ui/configAppearance.qml:37
#, kde-format
msgctxt "@option:check"
msgid "Caps Lock"
msgstr "Caps Lock"

#: contents/ui/configAppearance.qml:44
#, kde-format
msgctxt "@option:check"
msgid "Num Lock"
msgstr "Num Lock"

#: contents/ui/main.qml:30
#, kde-format
msgid "Caps Lock activated\n"
msgstr "Caps Lock activado\n"

#: contents/ui/main.qml:31
#, kde-format
msgid "Num Lock activated\n"
msgstr "Num Lock activado\n"

#: contents/ui/main.qml:111
#, kde-format
msgid "No lock keys activated"
msgstr "Nenhuma tecla bloqueada activa"
