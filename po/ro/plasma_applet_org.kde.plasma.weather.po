# Traducerea plasma_applet_weather.po în Română
# Copyright (C) 2008 This_file_is_part_of_KDE
# This file is distributed under the same license as the plasma_applet_weather package.
# Laurenţiu Buzdugan <lbuz@rolix.org>, 2008".
# Sergiu Bivol <sergiu@cip.md>, 2008, 2009, 2020, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_weather\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-26 00:47+0000\n"
"PO-Revision-Date: 2022-08-14 16:49+0100\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.3\n"

#: i18n.dat:1
#, kde-format
msgctxt "wind direction"
msgid "N"
msgstr "N"

#: i18n.dat:2
#, kde-format
msgctxt "wind direction"
msgid "NNE"
msgstr "NNE"

#: i18n.dat:3
#, kde-format
msgctxt "wind direction"
msgid "NE"
msgstr "NE"

#: i18n.dat:4
#, kde-format
msgctxt "wind direction"
msgid "ENE"
msgstr "ENE"

#: i18n.dat:5
#, kde-format
msgctxt "wind direction"
msgid "E"
msgstr "E"

#: i18n.dat:6
#, kde-format
msgctxt "wind direction"
msgid "SSE"
msgstr "SSE"

#: i18n.dat:7
#, kde-format
msgctxt "wind direction"
msgid "SE"
msgstr "SE"

#: i18n.dat:8
#, kde-format
msgctxt "wind direction"
msgid "ESE"
msgstr "ESE"

#: i18n.dat:9
#, kde-format
msgctxt "wind direction"
msgid "S"
msgstr "S"

#: i18n.dat:10
#, kde-format
msgctxt "wind direction"
msgid "NNW"
msgstr "NNV"

#: i18n.dat:11
#, kde-format
msgctxt "wind direction"
msgid "NW"
msgstr "NV"

#: i18n.dat:12
#, kde-format
msgctxt "wind direction"
msgid "WNW"
msgstr "VNV"

#: i18n.dat:13
#, kde-format
msgctxt "wind direction"
msgid "W"
msgstr "V"

#: i18n.dat:14
#, kde-format
msgctxt "wind direction"
msgid "SSW"
msgstr "SSV"

#: i18n.dat:15
#, kde-format
msgctxt "wind direction"
msgid "SW"
msgstr "SV"

#: i18n.dat:16
#, kde-format
msgctxt "wind direction"
msgid "WSW"
msgstr "VSV"

#: i18n.dat:17
#, kde-format
msgctxt "wind direction"
msgid "VR"
msgstr ""

#: i18n.dat:18
#, kde-format
msgctxt "wind speed"
msgid "Calm"
msgstr "Calm"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Weather Station"
msgstr "Stație meteorologică"

#: package/contents/config/config.qml:19
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Aspect"

#: package/contents/config/config.qml:25
#, kde-format
msgctxt "@title"
msgid "Units"
msgstr "Unități"

#: package/contents/ui/config/ConfigAppearance.qml:36
#, kde-format
msgctxt "@title:group"
msgid "Compact Mode"
msgstr "Regim compact"

#: package/contents/ui/config/ConfigAppearance.qml:44
#, fuzzy, kde-format
#| msgid "&Temperature unit"
msgctxt "@label"
msgid "Show temperature:"
msgstr "Unitate &temperatură"

#: package/contents/ui/config/ConfigAppearance.qml:47
#, fuzzy, kde-format
#| msgctxt "@label"
#| msgid "Show beside widget icon:"
msgctxt "@option:radio Show temperature:"
msgid "Over the widget icon"
msgstr "Arată lângă pictograma controlului:"

#: package/contents/ui/config/ConfigAppearance.qml:55
#, fuzzy, kde-format
#| msgctxt "@label"
#| msgid "Show beside widget icon:"
msgctxt "@option:radio Show temperature:"
msgid "Beside the widget icon"
msgstr "Arată lângă pictograma controlului:"

#: package/contents/ui/config/ConfigAppearance.qml:62
#, kde-format
msgctxt "@option:radio Show temperature:"
msgid "Do not show"
msgstr ""

#: package/contents/ui/config/ConfigAppearance.qml:71
#, kde-format
msgctxt "@label"
msgid "Show in tooltip:"
msgstr "Arată în indiciu:"

#: package/contents/ui/config/ConfigAppearance.qml:72
#, kde-format
msgctxt "@option:check"
msgid "Temperature"
msgstr "Temperatură"

#: package/contents/ui/config/ConfigAppearance.qml:77
#, kde-format
msgctxt "@option:check Show in tooltip: wind"
msgid "Wind"
msgstr "Vânt"

#: package/contents/ui/config/ConfigAppearance.qml:82
#, kde-format
msgctxt "@option:check Show in tooltip: pressure"
msgid "Pressure"
msgstr "Presiune"

#: package/contents/ui/config/ConfigAppearance.qml:87
#, kde-format
msgctxt "@option:check Show in tooltip: humidity"
msgid "Humidity"
msgstr "Umiditate"

#: package/contents/ui/config/ConfigUnits.qml:33
#, kde-format
msgctxt "@label:listbox"
msgid "Temperature:"
msgstr "Temperatură:"

#: package/contents/ui/config/ConfigUnits.qml:39
#, kde-format
msgctxt "@label:listbox"
msgid "Pressure:"
msgstr "Presiune:"

#: package/contents/ui/config/ConfigUnits.qml:45
#, kde-format
msgctxt "@label:listbox"
msgid "Wind speed:"
msgstr "Viteza vântului:"

#: package/contents/ui/config/ConfigUnits.qml:51
#, kde-format
msgctxt "@label:listbox"
msgid "Visibility:"
msgstr "Vizibilitate:"

#: package/contents/ui/config/ConfigWeatherStation.qml:46
#, kde-format
msgctxt "@label:spinbox"
msgid "Update every:"
msgstr "Actualizare la fiecare:"

#: package/contents/ui/config/ConfigWeatherStation.qml:49
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minut"
msgstr[1] "%1 minute"
msgstr[2] "%1 de minute"

#: package/contents/ui/config/ConfigWeatherStation.qml:61
#, kde-format
msgctxt "@label"
msgid "Location:"
msgstr "Amplasare:"

#: package/contents/ui/config/ConfigWeatherStation.qml:67
#, kde-format
msgctxt "No location is currently selected"
msgid "None selected"
msgstr ""

#: package/contents/ui/config/ConfigWeatherStation.qml:71
#, kde-format
msgctxt "@label"
msgid "Provider:"
msgstr ""

#: package/contents/ui/config/ConfigWeatherStation.qml:95
#, fuzzy, kde-format
#| msgctxt "@info:placeholder"
#| msgid "Enter location"
msgctxt "@info:placeholder"
msgid "Enter new location"
msgstr "Introduceți locul"

#: package/contents/ui/config/ConfigWeatherStation.qml:95
#, kde-format
msgctxt "@info:placeholder"
msgid "Enter location"
msgstr "Introduceți locul"

#: package/contents/ui/config/ConfigWeatherStation.qml:188
#, kde-format
msgctxt "@info"
msgid "No weather stations found for '%1'"
msgstr "Nicio stație meteorologică găsită pentru „%1”"

#: package/contents/ui/config/ConfigWeatherStation.qml:190
#, kde-format
msgctxt "@info"
msgid "Search for a weather station to change your location"
msgstr ""

#: package/contents/ui/config/ConfigWeatherStation.qml:192
#, kde-format
msgctxt "@info"
msgid "Search for a weather station to set your location"
msgstr ""

#: package/contents/ui/ForecastView.qml:73
#: package/contents/ui/ForecastView.qml:80
#, kde-format
msgctxt "Short for no data available"
msgid "-"
msgstr "-"

#: package/contents/ui/FullRepresentation.qml:36
#, kde-format
msgid "Please set your location"
msgstr "Stabiliți amplasarea"

#: package/contents/ui/FullRepresentation.qml:39
#, kde-format
msgid "Set location…"
msgstr "Stabilește amplasarea…"

#: package/contents/ui/FullRepresentation.qml:54
#, kde-format
msgid "Weather information retrieval for %1 timed out."
msgstr ""

#: package/contents/ui/main.qml:86
#, kde-format
msgctxt "pressure tendency"
msgid "Rising"
msgstr "În creștere"

#: package/contents/ui/main.qml:87
#, kde-format
msgctxt "pressure tendency"
msgid "Falling"
msgstr "În scădere"

#: package/contents/ui/main.qml:88
#, kde-format
msgctxt "pressure tendency"
msgid "Steady"
msgstr "Stabilă"

#: package/contents/ui/main.qml:110
#, kde-format
msgctxt "Wind condition"
msgid "Calm"
msgstr "Calm"

#: package/contents/ui/main.qml:148
#, kde-format
msgctxt "Forecast period timeframe"
msgid "1 Day"
msgid_plural "%1 Days"
msgstr[0] "1 zi"
msgstr[1] "%1 zile"
msgstr[2] "%1 de zile"

#: package/contents/ui/main.qml:170
#, kde-format
msgctxt "@label"
msgid "Windchill:"
msgstr "Răcoare:"

#: package/contents/ui/main.qml:177
#, kde-format
msgctxt "@label"
msgid "Humidex:"
msgstr "Umidex:"

#: package/contents/ui/main.qml:184
#, kde-format
msgctxt "@label ground temperature"
msgid "Dewpoint:"
msgstr "Punct de condensare:"

#: package/contents/ui/main.qml:191
#, kde-format
msgctxt "@label"
msgid "Pressure:"
msgstr "Presiune:"

#: package/contents/ui/main.qml:198
#, kde-format
msgctxt "@label pressure tendency, rising/falling/steady"
msgid "Pressure Tendency:"
msgstr "Tendință presiune:"

#: package/contents/ui/main.qml:205
#, kde-format
msgctxt "@label"
msgid "Visibility:"
msgstr "Vizibilitate:"

#: package/contents/ui/main.qml:212
#, kde-format
msgctxt "@label"
msgid "Humidity:"
msgstr "Umiditate:"

#: package/contents/ui/main.qml:219
#, fuzzy, kde-format
#| msgctxt "winds exceeding wind speed briefly"
#| msgid "Wind Gust: %1 %2"
msgctxt "@label"
msgid "Wind Gust:"
msgstr "Rafale: %1 %2"

#: package/contents/ui/main.qml:239
#, fuzzy, kde-format
#| msgctxt "Forecast period timeframe"
#| msgid "1 Day"
#| msgid_plural "%1 Days"
msgctxt "Time of the day (from the duple Day/Night)"
msgid "Day"
msgstr "1 zi"

#: package/contents/ui/main.qml:240
#, kde-format
msgctxt "Time of the day (from the duple Day/Night)"
msgid "Night"
msgstr ""

#: package/contents/ui/main.qml:278
#, kde-format
msgctxt "certain weather condition (probability percentage)"
msgid "%1 (%2 %)"
msgstr "%1 (%2 %)"

#: package/contents/ui/main.qml:370
#, kde-format
msgctxt "@info:tooltip %1 is the translated plasmoid name"
msgid "Click to configure %1"
msgstr "Apăsați pentru a configura %1"

#: package/contents/ui/main.qml:381
#, kde-format
msgctxt "weather condition + temperature"
msgid "%1 %2"
msgstr "%1 %2"

#: package/contents/ui/main.qml:388
#, kde-format
msgctxt "winddirection windspeed (windgust)"
msgid "%1 %2 (%3)"
msgstr "%1 %2 (%3)"

#: package/contents/ui/main.qml:391
#, kde-format
msgctxt "winddirection windspeed"
msgid "%1 %2"
msgstr "%1 %2"

#: package/contents/ui/main.qml:400
#, kde-format
msgctxt "pressure (tendency)"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: package/contents/ui/main.qml:407
#, kde-format
msgid "Humidity: %1"
msgstr "Umiditate: %1"

#: package/contents/ui/NoticesView.qml:38
#, kde-format
msgctxt "@title:column weather warnings"
msgid "Warnings Issued"
msgstr "Avertizări emise"

#: package/contents/ui/NoticesView.qml:38
#, kde-format
msgctxt "@title:column weather watches"
msgid "Watches Issued"
msgstr "Prognoze emise"

#: package/contents/ui/SwitchPanel.qml:43
#, kde-format
msgctxt "@title:tab"
msgid "Details"
msgstr "Detalii"

#: package/contents/ui/SwitchPanel.qml:59
#, kde-format
msgctxt "@title:tab"
msgid "Notices"
msgstr "Notificări"

#: plugin/locationlistmodel.cpp:41 plugin/locationlistmodel.cpp:69
#, kde-format
msgid "Cannot find '%1' using %2."
msgstr "Nu se poate găsi „%1” folosind %2."

#: plugin/locationlistmodel.cpp:66
#, kde-format
msgid "Connection to %1 weather server timed out."
msgstr "Conexiunea la serverul meteorologic %1 a expirat."

#: plugin/locationlistmodel.cpp:125
#, kde-format
msgctxt "A weather station location and the weather service it comes from"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: plugin/util.cpp:43
#, kde-format
msgctxt "Degree, unit symbol"
msgid "°"
msgstr "°"

#: plugin/util.cpp:47 plugin/util.cpp:51
#, kde-format
msgctxt "temperature unitsymbol"
msgid "%1 %2"
msgstr "%1 %2"

#: plugin/util.cpp:61
#, kde-format
msgctxt "value unitsymbol"
msgid "%1 %2"
msgstr "%1 %2"

#: plugin/util.cpp:67
#, kde-format
msgctxt "value percentsymbol"
msgid "%1 %"
msgstr "%1 %"

#: plugin/util.cpp:73
#, kde-format
msgctxt "@item %1 is a unit description and %2 its unit symbol"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#~ msgctxt "@action:button"
#~ msgid "Choose…"
#~ msgstr "Alege…"

#~ msgctxt "@title:window"
#~ msgid "Select Weather Station"
#~ msgstr "Alege stația meteorologică"

#~ msgctxt "@action:button"
#~ msgid "Select"
#~ msgstr "Alege"

#~ msgctxt "@action:button"
#~ msgid "Cancel"
#~ msgstr "Renunță"

#~ msgctxt "@option:check Show on widget icon: temperature"
#~ msgid "Temperature"
#~ msgstr "Temperatură"

#~ msgctxt "@info:tooltip"
#~ msgid "Please configure"
#~ msgstr "Trebuie configurat"

#~ msgctxt "weather services provider name (id)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgid "Weather providers:"
#~ msgstr "Furnizori de date meteo:"

#~ msgctxt "@action:button"
#~ msgid "Search"
#~ msgstr "Caută"

#, fuzzy
#~| msgctxt "Shown when you have not set a weather provider"
#~| msgid "Please Configure"
#~ msgctxt "@action:button"
#~ msgid "Configure..."
#~ msgstr "Vă rugăm să configurați"

#, fuzzy
#~| msgid "Celsius"
#~ msgctxt "@item"
#~ msgid "Celsius °C"
#~ msgstr "Celsius"

#, fuzzy
#~| msgid "Fahrenheit"
#~ msgctxt "@item"
#~ msgid "Fahrenheit °F"
#~ msgstr "Fahrenheit"

#, fuzzy
#~| msgid "Kelvin"
#~ msgctxt "@item"
#~ msgid "Kelvin K"
#~ msgstr "Kelvin"

#, fuzzy
#~| msgid "Hectopascals (hPa)"
#~ msgctxt "@item"
#~ msgid "Hectopascals hPa"
#~ msgstr "Hectopascali (hPa)"

#, fuzzy
#~| msgid "Kilopascals (kPa)"
#~ msgctxt "@item"
#~ msgid "Kilopascals kPa"
#~ msgstr "Kilopascali (kPa)"

#, fuzzy
#~| msgid "Millibars (mb)"
#~ msgctxt "@item"
#~ msgid "Millibars mbar"
#~ msgstr "Millibari (mb)"

#, fuzzy
#~| msgid "Inches of Mercury (inHg)"
#~ msgctxt "@item"
#~ msgid "Inches of Mercury inHg"
#~ msgstr "Țoli pe coloana de mercur (inHg)"

#, fuzzy
#~| msgid "Metres Per Second (m/s)"
#~ msgctxt "@item"
#~ msgid "Meters per Second m/s"
#~ msgstr "Metri pe secundă (m/s)"

#, fuzzy
#~| msgid "Kilometers Per Hour (km/h)"
#~ msgctxt "@item"
#~ msgid "Kilometers per Hour km/h"
#~ msgstr "Kilometri pe oră (km/h)"

#, fuzzy
#~| msgid "Miles Per Hour (mph)"
#~ msgctxt "@item"
#~ msgid "Miles per Hour mph"
#~ msgstr "Mile pe oră (mph)"

#, fuzzy
#~| msgid "Knots (kt)"
#~ msgctxt "@item"
#~ msgid "Knots kt"
#~ msgstr "Noduri (kt)"

#, fuzzy
#~| msgid "Beaufort Scale"
#~ msgctxt "@item"
#~ msgid "Beaufort scale bft"
#~ msgstr "Scara Beaufort"

#, fuzzy
#~| msgid "Kilometers"
#~ msgctxt "@item"
#~ msgid "Kilometers"
#~ msgstr "Kilometri"

#, fuzzy
#~| msgid "Miles"
#~ msgctxt "@item"
#~ msgid "Miles"
#~ msgstr "Mile"

#, fuzzy
#~| msgctxt "content of water in air"
#~| msgid "Humidity: %1"
#~ msgctxt "@option:check"
#~ msgid "Show wind"
#~ msgstr "Umiditate: %1"

#, fuzzy
#~| msgid "&Pressure unit"
#~ msgctxt "@option:check"
#~ msgid "Show pressure"
#~ msgstr "Unitate &presiune"

#, fuzzy
#~| msgctxt "content of water in air"
#~| msgid "Humidity: %1"
#~ msgctxt "@option:check"
#~ msgid "Show humidity"
#~ msgstr "Umiditate: %1"

#, fuzzy
#~| msgctxt "Short for no data available"
#~| msgid "-"
#~ msgctxt "no weather station"
#~ msgid "-"
#~ msgstr "-"

#, fuzzy
#~| msgctxt "pressure, unit"
#~| msgid "Pressure: %1 %2"
#~ msgid "Pressure: %1"
#~ msgstr "Presiune: %1 %2"

#, fuzzy
#~| msgctxt "visibility from distance"
#~| msgid "Visibility: %1"
#~ msgid "Visibility: %1"
#~ msgstr "Vizibilitate: %1"

#, fuzzy
#~| msgctxt ""
#~| "%1 is the weather condition, %2 is the temperature, both come from the "
#~| "weather provider"
#~| msgid "%1 %2"
#~ msgctxt ""
#~ "%1 is the weather condition, %2 is the temperature,  both come from the "
#~ "weather provider"
#~ msgid "%1 %2"
#~ msgstr "%1 %2"

#~ msgctxt "High & Low temperature"
#~ msgid "H: %1 L: %2"
#~ msgstr "Î: %1 J: %2"

#~ msgctxt "Low temperature"
#~ msgid "Low: %1"
#~ msgstr "Joasă: %1"

#~ msgctxt "High temperature"
#~ msgid "High: %1"
#~ msgstr "Înaltă: %1"

#~ msgctxt "temperature, unit"
#~ msgid "%1%2"
#~ msgstr "%1%2"

#, fuzzy
#~| msgctxt "Not available"
#~| msgid "N/A"
#~ msgid "N/A"
#~ msgstr "Indisp."

#~ msgctxt "distance, unit"
#~ msgid "Visibility: %1 %2"
#~ msgstr "Vizibilitate: %1 %2"

#, fuzzy
#~| msgid "%1"
#~ msgctxt "Percent, measure unit"
#~ msgid "%"
#~ msgstr "%1"

#~ msgctxt "Not available"
#~ msgid "N/A"
#~ msgstr "Indisp."

#~ msgid "Found Places"
#~ msgstr "Locuri găsite"

#~ msgid "Found places"
#~ msgstr "Locuri găsite"

#~ msgid "Locations"
#~ msgstr "Locații"

#, fuzzy
#~| msgid "S&ource"
#~ msgid "S&ource:"
#~ msgstr "S&ursă"

#, fuzzy
#~| msgid "City"
#~ msgid "City:"
#~ msgstr "Oraș"

#, fuzzy
#~| msgid "&Wind unit"
#~ msgid "&Wind unit:"
#~ msgstr "Unitate &vânt"

#, fuzzy
#~| msgid "&Visibility unit"
#~ msgid "&Visibility unit:"
#~ msgstr "Unitate vi&zibilitate"

#~ msgid ""
#~ "The applet was not able to contact the server, please try again later"
#~ msgstr "Miniaplicația nu a putut contacta serverul, reîncercați mai târziu"

#~ msgid ""
#~ "The place '%1' is not valid. The data source is not able to find this "
#~ "place."
#~ msgstr "Locul „%1” nu este valid. Sursa de date nu a putut găsi acest loc."

#~ msgid "Invalid Place"
#~ msgstr "Loc nevalid"

#~ msgid "&Add"
#~ msgstr "&Adaugă"

#~ msgid "&Update Now"
#~ msgstr "Act&ualizează acum"

#~ msgctxt "wind direction, speed"
#~ msgid "%1 %2m/s"
#~ msgstr "%1 %2m/s"

#~ msgctxt "wind direction, speed"
#~ msgid "%1 %2kt"
#~ msgstr "%1 %2kt"

#~ msgctxt "wind direction, speed"
#~ msgid "%1 %2bft"
#~ msgstr "%1 %2bft"

#~ msgctxt "wind direction, speed"
#~ msgid "%1 %2mph"
#~ msgstr "%1 %2mph"

#~ msgctxt "wind direction, speed"
#~ msgid "%1 %2km/h"
#~ msgstr "%1 %2km/h"

#~ msgid "Data Source"
#~ msgstr "Sursă date"

#~ msgid "General"
#~ msgstr "General"

#, fuzzy
#~| msgid "&Search"
#~ msgid "Search Place"
#~ msgstr "&Căutare"

#, fuzzy
#~| msgid "Place"
#~ msgid "&Place"
#~ msgstr "Loc"

#~ msgid "Select data source:"
#~ msgstr "Alegeți sursa datelor:"

#~ msgid "Type in a location:"
#~ msgstr "Introduceți o locație:"

#~ msgid "&Remove Place"
#~ msgstr "Eliminare &loc"

#~ msgid "Update weather information every"
#~ msgstr "Actualizare informații despre vreme la fiecare"

#~ msgid "Select wind format:"
#~ msgstr "Alegeți formatul vântului:"

#~ msgid "Plugin"
#~ msgstr "Modul"

#~ msgid "Wind Direction: %1"
#~ msgstr "Direcția vântului: %1"

#, fuzzy
#~| msgid "Windchill: %1"
#~ msgid "Wind: %1%2"
#~ msgstr "Răcoare: %1"

#~ msgid "Please wait, loading weather information..."
#~ msgstr "Așteptați, încarc informația despre vreme..."

#~ msgid "No weather locations specified yet. Please configure"
#~ msgstr "Nu sunt locații specificate. Configurați, vă rog."

#~ msgid "Weather Conditions for %1, %2"
#~ msgstr "Condiții meteorologice pentru %1, %2"

#~ msgid "Current Weather: %1"
#~ msgstr "Vreme curentă: %1"
