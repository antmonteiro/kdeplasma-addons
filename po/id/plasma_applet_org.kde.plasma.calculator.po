# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# wantoyo <wantoyek@gmail.com>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-18 00:58+0000\n"
"PO-Revision-Date: 2018-07-22 10:36+0700\n"
"Last-Translator: wantoyo <wantoyek@gmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: package/contents/ui/calculator.qml:320
#, kde-format
msgctxt "@label calculation result"
msgid "Result"
msgstr ""

#: package/contents/ui/calculator.qml:343
#, kde-format
msgctxt "Text of the clear button"
msgid "C"
msgstr "C"

#: package/contents/ui/calculator.qml:356
#, kde-format
msgctxt "Text of the division button"
msgid "÷"
msgstr "÷"

#: package/contents/ui/calculator.qml:369
#, kde-format
msgctxt "Text of the multiplication button"
msgid "×"
msgstr "×"

#: package/contents/ui/calculator.qml:381
#, kde-format
msgctxt "Text of the all clear button"
msgid "AC"
msgstr "AC"

#: package/contents/ui/calculator.qml:433
#, kde-format
msgctxt "Text of the minus button"
msgid "-"
msgstr "-"

#: package/contents/ui/calculator.qml:485
#, kde-format
msgctxt "Text of the plus button"
msgid "+"
msgstr "+"

#: package/contents/ui/calculator.qml:536
#, kde-format
msgctxt "Text of the equals button"
msgid "="
msgstr "="
